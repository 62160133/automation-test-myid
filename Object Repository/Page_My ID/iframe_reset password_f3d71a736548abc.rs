<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_reset password_f3d71a736548abc</name>
   <tag></tag>
   <elementGuidId>9652330e-8607-4512-9562-81d65714cf3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@name='f3d71a736548abc']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>iframe[name=&quot;f3d71a736548abc&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>f3d71a736548abc</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>dialog_iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>frameborder</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowtransparency</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>no</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allow</name>
      <type>Main</type>
      <value>encrypted-media</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://www.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df32d7abdcb8d7bc%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ff58f4cdea3482c%26relation%3Dparent.parent&amp;container_width=1017&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2F&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=207f6d61-c71e-42e9-9e87-5bd233888d52&amp;page_id=160067777401491&amp;request_time=1647168295652&amp;sdk=joey</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fb-root&quot;)/div[@class=&quot;fb_iframe_widget fb_invisible_flow&quot;]/span[1]/iframe[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@name='f3d71a736548abc']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fb-root']/div[2]/span/iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@name = 'f3d71a736548abc' and @src = 'https://www.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df32d7abdcb8d7bc%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ff58f4cdea3482c%26relation%3Dparent.parent&amp;container_width=1017&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2F&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=207f6d61-c71e-42e9-9e87-5bd233888d52&amp;page_id=160067777401491&amp;request_time=1647168295652&amp;sdk=joey']</value>
   </webElementXpaths>
</WebElementEntity>
