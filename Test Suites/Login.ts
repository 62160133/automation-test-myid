<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>18543111-59f0-41ed-94ba-b94ddb3f3c40</testSuiteGuid>
   <testCaseLink>
      <guid>e9859514-748d-4b3e-8239-abcd7b09b85b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Logout Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8844da87-0927-43aa-8eb0-88a9958c70a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>580ce9ab-6cb5-4b61-9006-a2aa2271fcc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73b1a89e-5f98-4d84-86f6-19cb626c7dba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45028ac4-37e9-4cb2-a2e7-8e3d43cae792</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Password Failed by without special characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95749a64-63b7-4c27-b03f-1eccb84ab198</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Password Failed by without numbers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e36c3dd1-a295-4f49-a049-f45e9564c924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Failed by without letters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6820bd32-d4e4-47e9-9f43-e855ef45b118</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Password Failed by with a lenght of less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5047770c-3699-4aa0-aa0d-b3d83135db2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Password Failed by the lenght is greater than 25</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
